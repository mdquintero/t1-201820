package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {



	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}

	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
		int val;
		if(bag!=null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				val = iter.next();
				if(val<min){
					min=val;
				}
			}
		}

		return min;
	}

	public int pairNumbers(IntegersBag bag){
		int x;
		int cont=0;
		if(bag!=null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				x=iter.next();
				if(x%2==0){
					cont++;
				}
			}
		}
		return cont;
	}

	public int sumAll(IntegersBag bag){
		int x=0;
		if(bag!=null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				x+=iter.next();
			}
		}
		return x;
	}
}
